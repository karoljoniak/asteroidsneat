import handler
from numpy import sin, cos, deg2rad, pi
from pygame.draw import circle
from detector import Detector
from asteroid import Asteroid


class Player:
    def __init__(self, neural_net):
        self.shoot = False
        self.time = 0
        self.dead = False
        self.neural_net = neural_net
        self.position = {"x": 400, "y": 300}
        self.direction = 0  # degree
        self.velocity = 0
        self.detector = Detector(num=8, radius=355, detector_x_pos=400, detector_y_pos=300)

    def tick(self, asteroids: [Asteroid]):
        # handling detector
        self.detector.set_position(self.position["x"], self.position["y"], self.direction)
        self.detector.scan(asteroids)

        # thinking
        if self.neural_net is not None:
            decisions = self.neural_net.activate(self.detector.distance)

        # calculate time
        self.time = self.time + 1 / 60

        # handling acceleration
        if self.neural_net is not None and decisions[0] > 0.5 and self.velocity < 10\
                or handler.up and self.velocity < 10:
            self.velocity = self.velocity + 0.25
        elif not handler.up and self.velocity > 0:
            self.velocity = self.velocity - 0.25

        # handling walls force field
        if self.position["y"] < 80:
            y_force = 10 - self.position["y"] / 8
        elif self.position["y"] > handler.screen_height - 80:
            y_force = -(10 - (handler.screen_height - self.position["y"]) / 8)
        else:
            y_force = 0

        if self.position["x"] < 80:
            x_force = 10 - self.position["x"] / 8
        elif self.position["x"] > handler.screen_width - 80:
            x_force = -(10 - (handler.screen_width - self.position["x"]) / 8)
        else:
            x_force = 0

        # handling movement
        if self.velocity > 0:
            self.position["x"] = self.position["x"] + cos(deg2rad(self.direction)) * self.velocity + x_force

            self.position["y"] = self.position["y"] + sin(deg2rad(self.direction)) * self.velocity + y_force

        # handling rotation
        if self.neural_net is not None and decisions[1] > 0.5 or handler.left:
            self.direction = self.direction - 5
        elif self.neural_net is not None and decisions[2] > 0.5 or handler.right:
            self.direction = self.direction + 5
        if self.direction < 0:
            self.direction = self.direction + 360
        if self.direction > 360:
            self.direction = self.direction - 360

        # handling shoot flag
        if self.neural_net is not None and decisions[3] > 0.5 or handler.space:
            self.shoot = True

    def render(self):

        self.detector.render()
        if self.dead:
            color = (255, 0, 0)
        else:
            color = (255, 255, 255)

        circle(handler.display, color, (self.position["x"], self.position["y"]), 3)

        x = round(self.position["x"] + 30 * cos(deg2rad(self.direction)))
        y = round(self.position["y"] + 30 * sin(deg2rad(self.direction)))

        circle(handler.display, (255, 0, 0), (x, y), 2)

    def erase(self):
        circle(handler.display, (0, 0, 0), (self.position["x"], self.position["y"]), 3)
        self.detector.erase()

        x = round(self.position["x"] + 30 * cos(deg2rad(self.direction)))
        y = round(self.position["y"] + 30 * sin(deg2rad(self.direction)))

        circle(handler.display, (0, 0, 0), (x, y), 2)
