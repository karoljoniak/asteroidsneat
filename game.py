import random

from player import Player
from asteroid import Asteroid
from bullet import Bullet
from random import randint, uniform
from numpy import arctan, pi
import handler


class Game:
    def __init__(self, neural_net, seed):
        self.player = Player(neural_net)
        self.asteroids: [Asteroid] = []
        self.bullet = Bullet()
        self.spawn_asteroid_delay = 0
        random.seed(seed)

    def tick(self):
        if self.spawn_asteroid_delay > 0:
            self.spawn_asteroid_delay = self.spawn_asteroid_delay - 1

        self.player.tick(self.asteroids)
        for a in self.asteroids:
            a.tick()
        self.handle_asteroids_logic()
        self.bullet.tick()
        if self.bullet.lifetime > 0:
            self.asteroids = self.bullet.handle_collision(self.asteroids)

        if self.bullet.reload_time == 0 and self.player.shoot:
            self.player.shoot = False
            x = self.player.position["x"]
            y = self.player.position["y"]
            direction = self.player.direction
            self.bullet.shoot(x, y, direction)

        self.handle_collision()

    def render(self):
        self.player.render()
        for a in self.asteroids:
            a.render()
        self.bullet.render()

    def erase(self):
        self.player.erase()
        for a in self.asteroids:
            a.erase()
        self.bullet.erase()

    def handle_collision(self):
        pos = self.player.position
        for a in self.asteroids:
            if ((pos["x"]-a.position["x"])**2 + (pos["y"]-a.position["y"])**2)**0.5 < a.radius:
                self.player.dead = True

    def add_asteroid(self):
        position = randint(5, 6)
        radius = randint(25, 60)
        velocity = uniform(1.5, 3)
        if position == 1:  # spawn asteroid on the top
            x = randint(0, handler.screen_width)
            y = -radius
        elif position == 2:  # spawn asteroid on the right
            x = handler.screen_width + radius
            y = randint(0, handler.screen_height)
        elif position == 3:  # spawn asteroid on the bottom
            x = randint(0, handler.screen_width)
            y = handler.screen_height + radius
        else:  # spawn asteroid on the left
            x = -radius
            y = randint(0, handler.screen_height)
        deg = self.calc_degree(x, self.player.position["x"], y, self.player.position["y"])
        self.asteroids.append(Asteroid(pos_x=x, pos_y=y, direction=deg, velocity=velocity, radius=radius))

    @staticmethod
    def calc_degree(x1, x2, y1, y2):
        if x2 != x1:
            val = (y2 - y1) / (x2 - x1)
            if x2 - x1 < 0:
                return arctan(val) + pi
            return arctan(val)
        elif y2 > y1:
            return pi
        else:
            return -pi

    @staticmethod
    def out_of_map(x, y, radius):
        return x > handler.screen_width + 2 * radius or x < 0 - 2 * radius or y < 0 - 2 * radius \
               or y > handler.screen_height + 2 * radius

    def handle_asteroids_logic(self):
        if self.spawn_asteroid_delay == 0:
            self.add_asteroid()
            self.spawn_asteroid_delay = len(self.asteroids) * 150

    def simulate(self):
        while not self.player.dead:
            self.tick()

        return self.player.time ** 2
