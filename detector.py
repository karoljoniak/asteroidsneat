from numpy import sin, cos, pi, deg2rad, round
import handler
from asteroid import Asteroid
from pygame.draw import circle, line


class Detector:
    def __init__(self, num, radius, detector_x_pos, detector_y_pos):
        self.num = num  # quantity of detectors
        self.radius = radius
        self.distance = [radius for _ in range(num)]
        self.position = {"x": detector_x_pos, "y": detector_y_pos}
        self.points = [
            {"x": detector_x_pos + radius * cos(2 * pi / num * x), "y": detector_y_pos + radius * sin(2 * pi / num * x)}
            for x in range(num)]

    def set_position(self, detector_x_pos, detector_y_pos, rotation):
        self.position = {"x": detector_x_pos, "y": detector_y_pos}
        self.points = [
            {"x": round(detector_x_pos + self.radius * cos(2 * pi / self.num * x + deg2rad(rotation))),
             "y": round(detector_y_pos + self.radius * sin(2 * pi / self.num * x + deg2rad(rotation)))} for x in
            range(self.num)]

    def erase(self):
        self.erase_points()

        for i in range(-1, len(self.distance) - 1):
            p1 = self.points[i - 1]
            p2 = self.points[i]
            p3 = self.position
            line(handler.display, (0, 0, 0), (p1["x"], p1["y"]), (p3["x"], p3["y"]), 2)
            line(handler.display, (0, 0, 0), (p2["x"], p2["y"]), (p3["x"], p3["y"]), 2)

    def render(self):
        self.render_points()

        for i in range(-1, len(self.distance) - 1):
            if self.distance[i] < self.radius:
                p1 = self.points[i - 1]
                p2 = self.points[i]
                p3 = self.position
                line(handler.display, (255 - self.distance[i] * 255, 0, 0), (p1["x"], p1["y"]), (p3["x"], p3["y"]), 2)
                line(handler.display, (255 - self.distance[i] * 255, 0, 0), (p2["x"], p2["y"]), (p3["x"], p3["y"]), 2)

    def render_points(self):
        for i in range(len(self.points)):
            p = self.points[i]
            if i == 0:
                circle(handler.display, (255, 0, 0), (p["x"], p["y"]), 1)
            else:
                circle(handler.display, (255, 255, 255), (p["x"], p["y"]), 1)

    def erase_points(self):
        for p in self.points:
            circle(handler.display, (0, 0, 0), (p["x"], p["y"]), 1)

    @staticmethod
    def __sign(p1, p2, p3):
        return (p1["x"] - p3["x"]) * (p2["y"] - p3["y"]) - (p2["x"] - p3["x"]) * (p1["y"] - p3["y"])

    @staticmethod
    def __point_in_triangle(pt, v1, v2, v3):
        d1 = Detector.__sign(pt, v1, v2)
        d2 = Detector.__sign(pt, v2, v3)
        d3 = Detector.__sign(pt, v3, v1)
        has_neg = (d1 < 0) or (d2 < 0) or (d3 < 0)
        has_pos = (d1 > 0) or (d2 > 0) or (d3 > 0)
        return not has_neg and has_pos

    def scan(self, asteroids: [Asteroid]):
        self.distance = [1 for _ in range(self.num)]  # reset scanner
        for a in asteroids:
            # check if asteroid is in detector radius
            if ((self.position["x"] - a.position["x"]) ** 2 + (
                    self.position["y"] - a.position["y"]) ** 2) ** 0.5 < self.radius:
                for d in range(-1, self.num - 1):
                    p1 = self.position
                    p2 = self.points[d]
                    p3 = self.points[d + 1]
                    if Detector.__point_in_triangle(a.position, p1, p2, p3):
                        distance = ((p1["x"] - a.position["x"]) ** 2 + (p1["y"] - a.position["y"]) ** 2) ** 0.5 / self.radius
                        if distance < self.distance[d + 1]:
                            self.distance[d + 1] = distance
                        continue  # if asteroid was assigned to right place, don't check the rest of places
