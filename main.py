from pygame.locals import *
import pygame as pg
from game import Game
import handler
import os
import neat


def show_graphic(net, seed):
    pg.init()
    pg.font.init()
    delta = 0
    clock = pg.time.Clock()

    game = Game(net, seed)

    while True:
        delta += clock.tick()
        if delta > 1000 / 60:
            delta -= 1000 / 60
        else:
            continue

        for event in pg.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pg.quit()
                exit(0)

            if event.type == pg.KEYDOWN:
                if event.key == pg.K_SPACE:
                    handler.space = True
                if event.key == pg.K_UP:
                    handler.up = True
                if event.key == pg.K_RIGHT:
                    handler.right = True
                if event.key == pg.K_LEFT:
                    handler.left = True

            if event.type == pg.KEYUP:
                if event.key == pg.K_SPACE:
                    handler.space = False
                if event.key == pg.K_UP:
                    handler.up = False
                if event.key == pg.K_RIGHT:
                    handler.right = False
                if event.key == pg.K_LEFT:
                    handler.left = False

        game.erase()
        game.tick()
        game.render()
        pg.display.update()


show_graphic(None, 10)
