from pygame.draw import circle
import handler
from numpy import sin, cos, pi


class Asteroid:
    def __init__(self, radius, direction, velocity, pos_x, pos_y):
        self.position = {"x": pos_x, "y": pos_y}
        self.direction = direction  # degree
        self.velocity = velocity
        self.radius = radius

    def tick(self):
        self.position["x"] = self.position["x"] + cos(self.direction) * self.velocity
        self.position["y"] = self.position["y"] + sin(self.direction) * self.velocity
        self.loopy()

    def render(self):
        circle(handler.display, (100, 100, 100), (self.position["x"], self.position["y"]), self.radius)

    def erase(self):
        circle(handler.display, (0, 0, 0), (self.position["x"], self.position["y"]), self.radius)

    def loopy(self):
        if self.position["x"] < - self.radius:
            self.position["x"] = handler.screen_width + self.radius
        elif self.position["x"] > handler.screen_width + self.radius:
            self.position["x"] = - self.radius
        elif self.position["y"] > handler.screen_height + self.radius:
            self.position["y"] = - self.radius
        elif self.position["y"] < - self.radius:
            self.position["y"] = handler.screen_height + self.radius
