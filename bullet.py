import handler
from numpy import sin, cos, deg2rad, ceil
from pygame.draw import circle
from asteroid import Asteroid


class Bullet:
    def __init__(self):
        self.position = {"x": 0, "y": 0}
        self.direction = 0  # degree
        self.velocity = 12
        self.lifetime = 0
        self.reload_time = 0

    def tick(self):
        if self.reload_time > 0:
            self.reload_time = self.reload_time - 1
        if self.lifetime > 0:
            self.position["x"] = self.position["x"] + cos(deg2rad(self.direction)) * self.velocity
            self.position["y"] = self.position["y"] + sin(deg2rad(self.direction)) * self.velocity
            self.lifetime = self.lifetime - 1

    def render(self):
        if self.lifetime > 0:
            circle(handler.display, (255, 255, 255), (self.position["x"], self.position["y"]), 3)

    def erase(self):
        if self.lifetime > 0:
            circle(handler.display, (0, 0, 0), (self.position["x"], self.position["y"]), 3)

    def shoot(self, x, y, direction):
        self.reload_time = 60
        self.lifetime = 60
        self.position = {"x": x, "y": y}
        self.direction = direction

    def handle_collision(self, asteroids: [Asteroid]):
        x = self.position["x"]
        y = self.position["y"]
        new_asteroids = []
        collision_detected = False
        for a in asteroids:
            ax = a.position["x"]
            ay = a.position["y"]

            if ((x - ax) ** 2 + (y - ay) ** 2) ** 0.5 < a.radius:
                collision_detected = True
                self.lifetime = 0
                if a.radius > 40:
                    new_asteroids.append(Asteroid(pos_x=ax, pos_y=ay, direction=a.direction + 0.2, velocity=a.velocity,
                                                  radius=ceil(a.radius / 3 * 2)))
                    new_asteroids.append(Asteroid(pos_x=ax, pos_y=ay, direction=a.direction - 0.2, velocity=a.velocity,
                                                  radius=ceil(a.radius / 3 * 2)))
            else:
                new_asteroids.append(a)

        if collision_detected:
            return new_asteroids
        else:
            return asteroids
